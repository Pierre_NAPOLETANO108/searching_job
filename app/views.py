from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.base import TemplateView
from django.contrib.auth.forms import UserCreationForm
from django.views import generic
from app.models import Company, JobOffer

# Common web pages
class HomeView(TemplateView):
    template_name = 'home.html'

    def get_context_data(self, *args, **kwargs):
        context = super(HomeView, self).get_context_data(*args, **kwargs)
        context['title'] = 'SearchingJob - Page d\'accueil'
        return context

class ContactView(TemplateView):
    template_name = 'contact.html'

    def get_context_data(self, *args, **kwargs):
        context = super(ContactView, self).get_context_data(*args, **kwargs)
        context['title'] = 'SearchingJob - Page de contact'
        return context

# Web pages linked to companies
class CompanyCreateView(CreateView):
    model = Company
    fields = ['logo', 'name', 'activity', 'city', 'zipcode', 'address', 'url', 'description']
    template_name = 'company/create_company.html'
    success_url = 'accueil'

    def get_context_data(self, *args, **kwargs):
        context = super(CompanyCreateView, self).get_context_data(*args, **kwargs)
        context['title'] = 'SearchingJob - Creation d\'une entreprise'
        return context

class CompanyDetailView(DetailView):
    model = Company
    template_name = 'company/detail_company.html'

    def get_context_data(self, *args, **kwargs):
        context = super(CompanyDetailView, self).get_context_data(*args, **kwargs)
        context['title'] = 'SearchingJob - Entreprise'
        return context

class CompanyUpdateView(UpdateView):
    model = Company
    fields = ('__all__')
    success_url = 'accueil'
    template_name = 'company/update_company.html'

    def get_context_data(self, *args, **kwargs):
        context = super(CompanyUpdateView, self).get_context_data(*args, **kwargs)
        context['title'] = 'SearchingJob - Edition d\'une entreprise'
        return context

class CompanyDeleteView(DeleteView):
    model = Company
    success_url = 'accueil'
    template_name = 'company/delete_company.html'

    def get_context_data(self, *args, **kwargs):
        context = super(CompanyDeleteView, self).get_context_data(*args, **kwargs)
        context['title'] = 'SearchingJob - Suppression d\'une entreprise'
        return context

class CompanyListView(ListView):
    model = Company
    template_name = 'company/list_company.html'

    def get_context_data(self, *args, **kwargs):
        context = super(CompanyListView, self).get_context_data(*args, **kwargs)
        context['title'] = 'SearchingJob - Liste des entreprises'
        return context

# Web pages linked to job offers
class JobOfferCreateView(CreateView):
    model = JobOffer
    fields = ('__all__')
    template_name = 'joboffer/create_joboffer.html'
    success_url = 'accueil'

    def get_context_data(self, *args, **kwargs):
        context = super(JobOfferCreateView, self).get_context_data(*args, **kwargs)
        context['title'] = 'SearchingJob - Creation d\'une offre d\'emploi'
        return context

class JobOfferDetailView(DetailView):
    model = JobOffer
    template_name = 'joboffer/detail_joboffer.html'

    def get_context_data(self, *args, **kwargs):
        context = super(JobOfferDetailView, self).get_context_data(*args, **kwargs)
        context['title'] = 'SearchingJob - Offre d\'emploi'
        return context

class JobOfferUpdateView(UpdateView):
    model = JobOffer
    fields = ('__all__')
    success_url = 'accueil'
    template_name = 'joboffer/update_joboffer.html'

    def get_context_data(self, *args, **kwargs):
        context = super(JobOfferUpdateView, self).get_context_data(*args, **kwargs)
        context['title'] = 'SearchingJob - Edition d\'une offre d\'emploi'
        return context

class JobOfferDeleteView(DeleteView):
    model = JobOffer
    success_url = 'accueil'
    template_name = 'joboffer/delete_joboffer.html'

    def get_context_data(self, *args, **kwargs):
        context = super(JobOfferDeleteView, self).get_context_data(*args, **kwargs)
        context['title'] = 'SearchingJob - Suppression d\'une offre d\'emploi'
        return context

class JobOfferListView(ListView):
    model = JobOffer
    template_name = 'joboffer/list_joboffer.html'
    
    def get_context_data(self, *args, **kwargs):
        context = super(JobOfferListView, self).get_context_data(*args, **kwargs)
        context['title'] = 'SearchingJob - Liste des offres d\'emploi postulé'
        return context

# Login and SignUp views

class SignUpView(generic.CreateView):
    form_class = UserCreationForm
    success_url = 'accueil'
    template_name = "registration/signup.html"

    def get_context_data(self, *args, **kwargs):
        context = super(SignUpView, self).get_context_data(*args, **kwargs)
        context['title'] = 'SearchingJob - Page d\'inscription'
        return context