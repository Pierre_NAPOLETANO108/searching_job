from django.db import models
from django.contrib.auth.models import User

class Company(models.Model):
    class Meta:
        verbose_name = 'Entreprise'
        verbose_name_plural = 'Entreprises'

    user = models.ForeignKey(User, on_delete=models.DO_NOTHING, blank=True, null=True)
    logo = models.ImageField(verbose_name='Logo de l\'entreprise', upload_to='companies/logos/')
    name = models.CharField(verbose_name='Nom de l\'entreprise', max_length=50)
    activity = models.CharField(verbose_name='Activit\xe9 de l\'entreprise', max_length=50)
    city = models.CharField(verbose_name='Ville', max_length=50)
    zipcode = models.CharField(verbose_name='Code postal', max_length=50)
    address = models.TextField(verbose_name='Adresse de l\'entreprise')
    url = models.URLField(verbose_name='Site web de l\'entreprise')
    description = models.TextField(verbose_name='Description de l\'entreprise')

    def __str__(self) -> str:
        return f'Entreprise: {self.name}'

class JobOffer(models.Model):
    class Meta:
        verbose_name = 'Offre d\'emploi'
        verbose_name_plural = 'Offres d\'emploi'

    class ApplicationStatusChoices(models.TextChoices):
        IN_PROGRESS = ('en cours', 'En cours')
        RELAUNCHED = ('relance', 'A relancé')
        RECEIVED_RESPONSE = ('reponse recu', 'A reçu une réponse')

    class ApplicationTypeChoices(models.TextChoices):
        INTERNSHIP = ('stage', 'Stage')
        SANDWICH_COURSE = ('alternance', 'Alternance')
        TEMPORARY_EMPLOYMENT = ('interim', 'Intérim')
        FIXED_TERM_CONTRACT = ('cdd', 'CDD')
        PERMANENT_CONTRACT = ('cdi', 'CDI')

    class ApplicationTypeSent(models.TextChoices):
        JOB_OFFER = ('offre d\'emploi', 'Offre d\'emploi')
        UNSOLICITED_APPLICATION = ('candidature spontanée', 'Candidature spontanée')

    user = models.ForeignKey(User, on_delete=models.DO_NOTHING, blank=True, null=True)
    company = models.ForeignKey(Company, on_delete=models.DO_NOTHING, blank=True, null=True)
    application_date = models.DateField(verbose_name='Date de candidature')
    application_status = models.CharField(verbose_name='Status de la candidature', max_length=50, choices=ApplicationStatusChoices.choices)
    application_type = models.CharField(verbose_name='Type de candidature', max_length=50, choices=ApplicationTypeChoices.choices)
    application_type_sent = models.CharField(verbose_name='Comment a été envoyée la candidature ?', max_length=50, choices=ApplicationTypeSent.choices)
    url_job_offer = models.URLField(verbose_name='URL de l\'offre d\'emploi', blank=True, null=True)
    date_dunning = models.DateField(verbose_name='Date de relance', blank=True, null=True)
    position_sought = models.CharField(verbose_name='Poste recherch\xe9', max_length=80, blank=True, null=True)
    contact_phone = models.CharField(verbose_name='T\xe9l\xe9phone du contact', max_length=50, blank=True, null=True)
    contact_email = models.CharField(verbose_name='Email du contact', max_length=50, blank=True, null=True)
    company_answer = models.CharField(verbose_name='Réponse de l\'entreprise', max_length=50, blank=True, null=True)

    def __str__(self) -> str:
        return f'Entreprise: {self.company.name} - Poste recherche: {self.position_sought}'