"""searching_job URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from app.views import HomeView, ContactView, CompanyCreateView, CompanyDetailView, CompanyUpdateView, CompanyDeleteView, CompanyListView, JobOfferCreateView, JobOfferDetailView, JobOfferUpdateView, JobOfferDeleteView, JobOfferListView, SignUpView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accueil/', HomeView.as_view(), name='home'),
    path('contact/', ContactView.as_view(), name='contact'),
    path('admininistration/creer-entreprise/', CompanyCreateView.as_view(), name='create_company'),
    path('admininistration/entreprise/<pk>', CompanyDetailView.as_view(), name='detail_company'),
    path('admininistration/editer-entreprise/<pk>', CompanyUpdateView.as_view(), name='update_company'),
    path('admininistration/supprimer-entreprise/<pk>', CompanyDeleteView.as_view(), name='delete_company'),
    path('admininistration/liste-entreprise/', CompanyListView.as_view(), name='list_company'),
    path('admininistration/creer-offre-emploi/', JobOfferCreateView.as_view(), name='create_joboffer'),
    path('admininistration/offre-emploi/<pk>', JobOfferDetailView.as_view(), name='detail_joboffer'),
    path('admininistration/editer-offre-emploi/<pk>', JobOfferUpdateView.as_view(), name='update_joboffer'),
    path('admininistration/supprimer-offre-emploi/<pk>', JobOfferDeleteView.as_view(), name='delete_joboffer'),
    path('admininistration/liste-offre-emploi/', JobOfferListView.as_view(), name='list_joboffer'),
    path('', include('django.contrib.auth.urls')),
    path('signup/', SignUpView.as_view(), name='signup'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)